package SMS_TERMINATE_MENU;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class MenuChose {

	public static void main(String[] args) throws Exception {
		Scanner in = new Scanner(System.in);

		// print menu
		menuPrint mp = new menuPrint();
		menuPrintASMS MPASMS = new menuPrintASMS();
		menuPrintTEMPEST MPTEMPEST= new menuPrintTEMPEST();
		menuPrintNETTRADE MPNETTRADE= new menuPrintNETTRADE();
		menuPrintTYNTEC MPTYNTEC = new menuPrintTYNTEC();
		boolean quit = false;
		String menuItem;

		do {
			
			mp.printMetod();
			System.out.print("Choose menu item: ");
			menuItem = in.next();
			switch (menuItem) {

			case "1":
				Scanner inASMS = new Scanner(System.in);
	            // print menu
				MPASMS.printMetod();
			      //////////////////////////ASMS SUBMENU///////////////////////////////////////////////
	            				boolean quitASMS = false;
	            				String ASMSsubmenu;
	            				do {
	            				System.out.print("Choose ASMS SUBmenu item: ");
	            				ASMSsubmenu = inASMS.next();
	            					switch (ASMSsubmenu) {
	            					case "1":
	            						System.out.println("ASMS ALL");
	            						SMS_TERMINATE_MENU.asmsAggregation.connect();
	            						SMS_TERMINATE_MENU.asms_fa_gen.connect();
	            						break;
	            					case "2":
	            						System.out.println("AGGREGATE ASMS");
	            						SMS_TERMINATE_MENU.asmsAggregation.connect();
	            						break;
	            					case "3":
	            						System.out.println("AGGREGATE INVOICE");
	            						SMS_TERMINATE_MENU.asms_fa_gen.connect();
	            						
	            						break;
	            					case "4":
	            						System.out.println("RERUN LAST MONTH");
	            						System.out.println("This possibility is still in development stage");
	            						break;
	                  				case "0":
	                  					quitASMS = true;
	                  					break;
	                  				default:
	                  					System.out.println("Invalid choice submenu.");
	                  }
	            } while (!quitASMS);
	            				mp.printMetod();
	            

	            				break;
	            				
	      //////////////////////////ASMS SUBMENU///////////////////////////////////////////////

			case "2":

				Scanner inTEMPEST = new Scanner(System.in);
	            // print menu
				MPTEMPEST.printMetod();
				 //////////////////////////////SUBMENU TEMPEST/////////////////////////////////////////////////    
	            				boolean quitTEMPEST = false;
	            				String TEMPESTsubmenu;
	            				do {
	            				System.out.print("Choose SUBmenu item: ");
	            				TEMPESTsubmenu = inTEMPEST.next();
	            					switch (TEMPESTsubmenu) {
	            					case "1":
	            						System.out.println("TEMPEST ALL");
	            						SMS_TERMINATE_MENU.tempestAggregation.connect();
	            						SMS_TERMINATE_MENU.tempest_fa_gen.connect();
	            						break;
	            					case "2":
	            						System.out.println("TEMPEST AGGREGATION ");
	            						SMS_TERMINATE_MENU.tempest_fa_gen.connect();
	            						break;
	            					case "3":
	            						System.out.println("TEMPEST INVOICE ");
	            						SMS_TERMINATE_MENU.tempest_fa_gen.connect();
	            						break;
	            					case "4":
	            						System.out.println("RERUN LAST MONTH");
	            						System.out.println("This possibility is still in development stage");
	            						break;
	                  				case "0":
	                  					quitTEMPEST = true;
	                  					break;
	                  				default:
	                  					System.out.println("Invalid choice submenu.");
	                  }
	            } while (!quitTEMPEST);
	            				mp.printMetod();
	            

	            				break;
	        //////////////////////////////SUBMENU TEMPEST/////////////////////////////////////////////////    				
	            				
			case "3":

				Scanner inNETTRADE = new Scanner(System.in);
	            // print menu
				MPNETTRADE.printMetod();
				 //////////////////////////////SUBMENU NETTRADE/////////////////////////////////////////////////    
	            				boolean quitNETTRADE = false;
	            				String NETTRADEsubmenu;
	            				do {
	            				System.out.print("Choose SUBmenu item: ");
	            				NETTRADEsubmenu = inNETTRADE.next();
	            					switch (NETTRADEsubmenu) {
	            					case "1":
	            						System.out.println("NETTRADE ALL");
	            						SMS_TERMINATE_MENU.nettradeAggregation.connect();
	            						SMS_TERMINATE_MENU.nettrade_fa_gen.connect();
	            						break;
	            					case "2":
	            						System.out.println("NETTRADE AGGREGATION");
	            						SMS_TERMINATE_MENU.nettradeAggregation.connect();
	            						break;
	            					case "3":
	            						System.out.println("NETTRADE INVOICE");
	            						SMS_TERMINATE_MENU.nettrade_fa_gen.connect();
	            						break;
	            					case "4":
	            						System.out.println("RERUN LAST MONTH");
	            						System.out.println("This possibility is still in development stage");
	            						break;
	                  				case "0":
	                  					quitNETTRADE = true;
	                  					break;
	                  				default:
	                  					System.out.println("Invalid choice submenu.");
	                  }
	            } while (!quitNETTRADE);
	            				mp.printMetod();
	            

	            				break;
	        //////////////////////////////SUBMENU NETTRADE/////////////////////////////////////////////////    	

				case "4":

				Scanner inTYNTEC = new Scanner(System.in);
	            // print menu
				MPTYNTEC.printMetod();
				 //////////////////////////////SUBMENU NETTRADE/////////////////////////////////////////////////    
	            				boolean quitTYNTEC= false;
	            				String TYNTECsubmenu;
	            				do {
	            				System.out.print("Choose SUBmenu item: ");
	            				TYNTECsubmenu = inTYNTEC.next();
	            					switch (TYNTECsubmenu) {
	            					case "1":
	            						System.out.println("TYNTEC ALL");
	            						SMS_TERMINATE_MENU.tyntecAggregation.connect();
	            						SMS_TERMINATE_MENU.tyntec_fa_gen.connect();
	            						break;
	            					case "2":
	            						System.out.println("TYNTEC AGGREGATION");
	            						SMS_TERMINATE_MENU.tyntecAggregation.connect();
	            						break;
	            					case "3":
	            						System.out.println("TYNTEC INVOICE");
	            						SMS_TERMINATE_MENU.tyntec_fa_gen.connect();
	            						break;
	            					case "4":
	            						System.out.println("RERUN LAST MONTH");
	            						System.out.println("This possibility is still in development stage");
	            						break;
	                  				case "0":
	                  					quitTYNTEC = true;
	                  					break;
	                  				default:
	                  					System.out.println("Invalid choice submenu.");
	                  }
	            } while (!quitTYNTEC);
	            				mp.printMetod();
	            

	            				break;
	        //////////////////////////////SUBMENU NETTRADE/////////////////////////////////////////////////    	
			case "5":

				System.out.println("Bics A2P_P2P invoice generation in proces");

				SMS_TERMINATE_MENU.bicsA2Pp2p.connect();

				break;
			case "6":

				System.out.println("Monty A2P_P2P invoice generation in proces");

				SMS_TERMINATE_MENU.montyA2Pp2p.connect();

				break;
			case "7":

				
				System.out.println("Generate all at once  - could run for a while");
							///ASMS///
				System.out.println( "ASMS ALL start");
				SMS_TERMINATE_MENU.asmsAggregation.connect();
				SMS_TERMINATE_MENU.asms_fa_gen.connect();
				System.out.println( "ASMS ALL done");
							//TEMPEST //
				System.out.println( "TEMPEST ALL start");
				SMS_TERMINATE_MENU.tempestAggregation.connect();
				SMS_TERMINATE_MENU.tempest_fa_gen.connect();
				System.out.println( "TEMPEST ALL done");
							//NETTRADE//
				System.out.println("NETTRADE ALL start");
				SMS_TERMINATE_MENU.nettradeAggregation.connect();
				SMS_TERMINATE_MENU.nettrade_fa_gen.connect();
				System.out.println("NETTRADE ALL done");
							//TYNTEC//
				System.out.println("TYNTEC ALL start");
				SMS_TERMINATE_MENU.tyntecAggregation.connect();
				SMS_TERMINATE_MENU.tyntec_fa_gen.connect();
				System.out.println("TYNTEC ALL done");
							//bicsA2Pp2p//
				System.out.println("bicsA2Pp2p ALL start");
				SMS_TERMINATE_MENU.bicsA2Pp2p.connect();
				System.out.println("bicsA2Pp2p ALL start");
							//bicsA2Pp2p//
				System.out.println("montyA2Pp2p ALL start");
				SMS_TERMINATE_MENU.montyA2Pp2p.connect();
				System.out.println("montyA2Pp2p ALL start");
				
				break;
			case "8":

				System.out.println("You've chosen item #8");

				showGeneratedfiles sf = new showGeneratedfiles();
				sf.printFileNames();

				break;
				
/*			case "9":

				System.out.println("You've chosen item #9");

				sendMail sm = new SMS_TRANSIT_MENU.sendMail();
						sm.sendMailReports();
				break;*/


			case "0":

				quit = true;
				in.close();

				break;

			default:

				System.out.println("Invalid choice.");

			}

		} while (!quit);

		System.out.println("Bye-bye!");
		

	}
}

class menuPrint{
	 menuPrint(){
	}
	public void printMetod(){
       System.out.println("");
		System.out.println("1. ASMS  ");
		System.out.println("2. TEMPEST");
		System.out.println("3. NETRADE");
		System.out.println("4. TYNTEC");
		System.out.println("5. bicsA2Pp2p");
		System.out.println("6. montyA2Pp2p");
		System.out.println("7. GENERATE ALL INVOICES AT ONCE");
		System.out.println("8. Show generated invoices");
		//System.out.println("9. Send mail");
		System.out.println("0. Quit");
	}
	}

 class menuPrintASMS{
	 menuPrintASMS(){
	}
	public void printMetod(){
        System.out.println("");
		System.out.println("1. ASMS ALL ");
		System.out.println("2. ASMS aggregation");
		System.out.println("3. ASMS invoice generation");
	//	System.out.println("4. ASMS invoice rerun ");
		System.out.println("0. Quit");
	}
	}
 
 class menuPrintTEMPEST{
	 menuPrintTEMPEST(){
	}
	public void printMetod(){
        System.out.println("");
		System.out.println("1. TEMPEST ALL ");
		System.out.println("2. TEMPEST aggregation");
		System.out.println("3. TEMPEST invoice generation");
	//	System.out.println("4. TEMPEST invoice rerun ");
		System.out.println("0. Quit");
	}
	}
 
 class menuPrintNETTRADE{
	 menuPrintNETTRADE(){
	}
	public void printMetod(){
        System.out.println("");
		System.out.println("1. NETTRADE ALL ");
		System.out.println("2. NETTRADE aggregation");
		System.out.println("3. NETTRADE invoice generation");
		//System.out.println("4. NETTRADE invoice rerun ");
		System.out.println("0. Quit");
	}
	}
 
 class menuPrintTYNTEC{
	 menuPrintTYNTEC(){
	}
	public void printMetod(){
        System.out.println("");
		System.out.println("1. TYNTEC ALL ");
		System.out.println("2. TYNTEC aggregation");
		System.out.println("3. TYNTEC invoice generation");
		//System.out.println("4. TYNTEC invoice rerun ");
		System.out.println("0. Quit");
	}
	}
	
