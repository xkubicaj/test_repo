package SMS_TERMINATE_MENU;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class montyA2Pp2p {

	public static Connection connect() {
		Properties result = null;
		InputStream in = null;
		in = ClassLoader.getSystemClassLoader().getResourceAsStream("db.properties");

		if (in != null) {
			result = new Properties();
			try {
				result.load(in);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} // Can throw IOException

		}

		String pguser = result.get("postgresuser").toString();
		String pwd = result.get("postgrespw").toString();
		String host = result.get("postgeshost").toString();
		
		String url = "jdbc:postgresql://"+ host +"/pgict1";
		String user = pguser;
		String password = pwd;
	Connection conn = null;
    DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
	Date date = new Date();
    
    
    try {	        	
    	
    	String readRecordSQL =
    	       " select  to_char(cast(date_trunc('day',r.billing_period) as date),'mm.yyyy') as billing_period,r.msgtype,count,r.unit_price,r.price " +
    		   " from ( " +
    			"		SELECT  date_trunc('month',recvtime)  as billing_period, 'P2P' as msgtype,count(*) as count,'0.028'  as unit_price, (count(*)*0.028) as price " +
    			"		FROM bssdata.imgwrecord t " +
    			"		where date_trunc('month',recvtime) = date_trunc('month', NOW()) - '1 month'::interval " +
    			"		and t.cpid  in ('507','508') " +
    			"		group by date_trunc('month',recvtime) " +
    			"		union " +
    			"		SELECT  date_trunc('month',recvtime)  as billing_period, 'A2P' as msgtype,count(*) as count,'0.028'  as unit_price,(count(*)*0.028) as price " +
    			"		FROM bssdata.imgwrecord t " +
    			"		where date_trunc('month',recvtime) = date_trunc('month', NOW()) - '1 month'::interval " +
    			"		and t.cpid  in ('517','518') " +
    			"		group by date_trunc('month',recvtime) " +
    			"		)r ";
    				                                                                                                                                   


    	conn = DriverManager.getConnection(url, user, password);
        Statement sqlStatement2 = conn.createStatement();
		ResultSet myResultSet2 = sqlStatement2.executeQuery(readRecordSQL);
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("MONTY_INVOICE_" + dateFormat.format(date));
		XSSFFont font = workbook.createFont();
		    font.setBold(true);
		    XSSFRow rowhead1 = sheet.createRow((short) 0);
		    rowhead1.createCell((short) 0).setCellValue("MONTY INVOICE");
		    XSSFRow rowhead = sheet.createRow((short) 1);
		    rowhead.createCell((short) 0).setCellValue("billing_period");
		    rowhead.createCell((short) 1).setCellValue("msgtype");
		    rowhead.createCell((short) 2).setCellValue("count");
		    rowhead.createCell((short) 3).setCellValue("unit_price");
		    rowhead.createCell((short) 4).setCellValue("price");
		    
		    int i = 2;
		    double total_price_fin = 0;
		    double total_price = 0;
		while (myResultSet2.next()) {
			XSSFRow row = sheet.createRow((short) i);
	        row.createCell((short) 0).setCellValue(myResultSet2.getString("billing_period"));
	        row.createCell((short) 1).setCellValue(myResultSet2.getString("msgtype"));
	        row.createCell((short) 2).setCellValue(myResultSet2.getString("count"));
	       	row.createCell((short) 3).setCellValue(Double.parseDouble(myResultSet2.getString("unit_price")));
	        
	        if (myResultSet2.getString("price") != null){
	        row.createCell((short) 4).setCellValue(Double.parseDouble(myResultSet2.getString("price")));
	        }else {
	        	row.createCell((short) 4).setCellValue(Double.parseDouble("0"));
	        }
	       
	        if(myResultSet2.getString("price") != null ){
	        	total_price = Double.parseDouble(myResultSet2.getString("price"));
	        }else{
	        	total_price = 0;
	        }
	       
	        total_price_fin = total_price_fin+ total_price;
	         
	        i++;
		}
		int num = sheet.getLastRowNum();
		XSSFRow row = sheet.createRow((short) num +2);
		row.createCell((short)3).setCellValue("total_price");
		row.createCell((short)4).setCellValue(total_price_fin);
	    System.out.println("BICS total price: " + total_price_fin);
		  String yemi = "REPORTS/MONTY_INVOICE_" +dateFormat.format(date) +".xlsx";
		    FileOutputStream fileOut = new FileOutputStream(yemi);
		    workbook.write(fileOut);
		    fileOut.close();
		    myResultSet2.close();
		    
		   
		conn.close();
		
		
		sheet.autoSizeColumn(0); 
		sheet.autoSizeColumn(1);
		sheet.autoSizeColumn(2); 
		sheet.autoSizeColumn(3); 
		sheet.autoSizeColumn(4); 
		sheet.autoSizeColumn(5); 
		sheet.autoSizeColumn(6); 
		sheet.autoSizeColumn(7); 
		workbook.close();
        
    } catch (SQLException e1) {
        e1.printStackTrace();
    } catch (FileNotFoundException e1) {
        e1.printStackTrace();
    } catch (IOException e1) {
        e1.printStackTrace();
    }

    return conn;
}

	}

	
	
	

