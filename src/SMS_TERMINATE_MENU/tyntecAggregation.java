package SMS_TERMINATE_MENU;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class tyntecAggregation {

	    public static Connection connect() {
	    	Properties result = null;
			InputStream in = null;
			in = ClassLoader.getSystemClassLoader().getResourceAsStream("db.properties");

			if (in != null) {
				result = new Properties();
				try {
					result.load(in);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} // Can throw IOException

			}

			String pguser = result.get("postgresuser").toString();
			String pwd = result.get("postgrespw").toString();
			String host = result.get("postgeshost").toString();
			
			String url = "jdbc:postgresql://"+ host +"/pgict1";
			String user = pguser;
			String password = pwd;
	        Connection conn = null;
	        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
			Date date = new Date();
	        
	        
	        try {
	        	String callFunction = "select pgict1oper.agregate_tyntec()";

	        	conn = DriverManager.getConnection(url, user, password);
	                  
	            Statement sqlStatement1 = conn.createStatement();
	            System.out.println("Aggregation start :" + date);
				ResultSet myResultSet1 = sqlStatement1.executeQuery(callFunction);
				myResultSet1.close();
				System.out.println("Aggregation of tyntec records has finished correctly : " + date);
				   
				conn.close();

	        } catch (SQLException e1) {
	            e1.printStackTrace();

	        }
	 
	        return conn;
	    }
}
