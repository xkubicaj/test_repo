package SMS_TERMINATE_MENU;

	import java.io.FileNotFoundException;
	import java.io.FileOutputStream;
	import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
	import java.sql.DriverManager;
	import java.sql.ResultSet;
	import java.sql.SQLException;
	import java.sql.Statement;
	import java.text.DateFormat;
	import java.text.SimpleDateFormat;
	import java.util.Date;
import java.util.Properties;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;
	import org.apache.poi.xssf.usermodel.XSSFRow;
	import org.apache.poi.xssf.usermodel.XSSFSheet;
	import org.apache.poi.xssf.usermodel.XSSFWorkbook;

	public class asms_fa_gen {

		    public static Connection connect() {
		    	
		    	Properties result = null;
				InputStream in = null;
				in = ClassLoader.getSystemClassLoader().getResourceAsStream("db.properties");

				if (in != null) {
					result = new Properties();
					try {
						result.load(in);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} // Can throw IOException

				}

				String pguser = result.get("postgresuser").toString();
				String pwd = result.get("postgrespw").toString();
				String host = result.get("postgeshost").toString();
				
				  String url = "jdbc:postgresql://"+ host +"/pgict1";
				  String user = pguser;
				  String password = pwd;
		        Connection conn = null;
		        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
				Date date = new Date();
		        
		        
		        try {

		        	String readRecordSQL =

		        			" SELECT to_char(cast(MO_MT_FINAL.billing_month AS DATE),'mm.yyyy') as billing_period                                                            " +
		        			"         ,msgtype                                                                                                                               " +
		        			"         ,count_of_SMS as count                                                                                                                 " +
		        			"         ,zone_cd as zone                                                                                                                       " +
		        			"         ,country_and_operator as destination                                                                                                   " +
		        			"         ,price_per_sms as unit_price                                                                                                           " +
		        			"         ,round((MO_MT_FINAL.count_of_SMS * MO_MT_FINAL.price_per_sms),2) AS price                                                              " +
		        			" FROM (                                                                                                                                         " +
		        			"         SELECT date_trunc('month', billing_date) AS billing_month                                                                              " +
		        			"                 ,aggregation.msgtype msgtype                                                                                                   " +
		        			"                 ,sum(rec_succesfull) AS count_of_SMS                                                                                           " +
		        			"                 ,zone_cd                                                                                                                       " +
		        			"                 ,country_and_operator                                                                                                          " +
		        			"                 ,CASE                                                                                                                          " +
		        			"                         WHEN country_and_operator = 'SWAN Mobile'                                                                              " +
		        			"                                 AND zone_cd = 'HOME'                                                                                           " +
		        			"                                 THEN (                                                                                                         " +
		        			"                                                 SELECT price                                                                                   " +
		        			"                                                 FROM pgict1oper.t_ocs_sms_conf_rates t_ocs_sms_conf_rates                                      " +
		        			"                                                 WHERE t_ocs_sms_conf_rates.network_service_desc = 'ASMS ONNET MT PRICE'                        " +
		        			"                                                 )                                                                                              " +
		        			"                         WHEN country_and_operator != 'SWAN Mobile'                                                                             " +
		        			"                                 AND zone_cd = 'HOME'                                                                                           " +
		        			"                                 THEN (                                                                                                         " +
		        			"                                                 SELECT price                                                                                   " +
		        			"                                                 FROM pgict1oper.t_ocs_sms_conf_rates t_ocs_sms_conf_rates                                      " +
		        			"                                                 WHERE t_ocs_sms_conf_rates.network_service_desc = 'ASMS OFFNET MT PRICE'                       " +
		        			"                                                 )                                                                                              " +
		        			"                         END AS price_per_SMS                                                                                                   " +
		        			"         FROM t_asms_day_aggregation aggregation                                                                                                " +
		        			"         WHERE aggregation.msgtype = 'MT'                                                                                                       " +
		        			"         GROUP BY date_trunc('month', billing_date)                                                                                             " +
		        			"                 ,zone_cd                                                                                                                       " +
		        			"                 ,aggregation.msgtype                                                                                                           " +
		        			"                 ,country_and_operator                                                                                                          " +
		        			"         UNION ALL                                                                                                                              " +
		        			"         SELECT date_trunc('month', billing_date) AS billing_month                                                                              " +
		        			"                 ,aggregation.msgtype msgtype                                                                                                   " +
		        			"                 ,sum(rec_succesfull) AS count_of_SMS                                                                                           " +
		        			"                 ,zone_cd                                                                                                                       " +
		        			"                 ,country_and_operator                                                                                                          " +
		        			"                 ,'0' AS price_per_SMS                                                                                                          " +
		        			"         FROM t_asms_day_aggregation aggregation                                                                                                " +
		        			"         WHERE aggregation.msgtype = 'MO'                                                                                                       " +
		        			"         GROUP BY date_trunc('month', billing_date)                                                                                             " +
		        			"                 ,zone_cd                                                                                                                       " +
		        			"                 ,aggregation.msgtype                                                                                                           " +
		        			"                 ,country_and_operator                                                                                                          " +
		        			"         UNION ALL                                                                                                                              " +
		        			"         SELECT date_trunc('month', billing_date) AS billing_month                                                                              " +
		        			"                 ,aggregation.msgtype msgtype                                                                                                   " +
		        			"                 ,sum(rec_succesfull) AS count_of_SMS                                                                                           " +
		        			"                 ,zone_cd                                                                                                                       " +
		        			"                 ,country_and_operator                                                                                                          " +
		        			"                 ,CASE                                                                                                                          " +
		        			"                         WHEN zone_cd = 'ZONE1'                                                                                                 " +
		        			"                                 THEN (                                                                                                         " +
		        			"                                                 SELECT t_ocs_sms_conf_rates.price / 1.2                                                        " +
		        			"                                                 FROM pgict1oper.t_ocs_sms_conf_rates t_ocs_sms_conf_rates                                      " +
		        			"                                                 WHERE t_ocs_sms_conf_rates.lookup_dest = 'HOME'                                                " +
		        			"                                                         AND t_ocs_sms_conf_rates.network_service_desc = 'MO SMS International Zone 1'          " +
		        			"                                                 )                                                                                              " +
		        			"                         WHEN zone_cd = 'ZONE2'                                                                                                 " +
		        			"                                 THEN (                                                                                                         " +
		        			"                                                 SELECT t_ocs_sms_conf_rates.price / 1.2                                                        " +
		        			"                                                 FROM pgict1oper.t_ocs_sms_conf_rates t_ocs_sms_conf_rates                                      " +
		        			"                                                 WHERE t_ocs_sms_conf_rates.zone_cd = 'HOME'                                                    " +
		        			"                                                         AND t_ocs_sms_conf_rates.network_service_desc = 'MO SMS International Zone 2'          " +
		        			"                                                 )                                                                                              " +
		        			"                         WHEN zone_cd = 'ZONE3'                                                                                                 " +
		        			"                                 THEN (                                                                                                         " +
		        			"                                                 SELECT t_ocs_sms_conf_rates.price / 1.2                                                        " +
		        			"                                                 FROM pgict1oper.t_ocs_sms_conf_rates t_ocs_sms_conf_rates                                      " +
		        			"                                                 WHERE t_ocs_sms_conf_rates.zone_cd = 'HOME'                                                    " +
		        			"                                                         AND t_ocs_sms_conf_rates.network_service_desc = 'MO SMS International Zone 3'          " +
		        			"                                                 )                                                                                              " +
		        			"                         WHEN zone_cd = 'ZONE4'                                                                                                 " +
		        			"                                 THEN (                                                                                                         " +
		        			"                                                 SELECT t_ocs_sms_conf_rates.price / 1.2                                                        " +
		        			"                                                 FROM pgict1oper.t_ocs_sms_conf_rates t_ocs_sms_conf_rates                                      " +
		        			"                                                 WHERE t_ocs_sms_conf_rates.zone_cd = 'HOME'                                                    " +
		        			"                                                         AND t_ocs_sms_conf_rates.network_service_desc = 'MO SMS International Zone 4'          " +
		        			"                                                 )                                                                                              " +
		        			"                         END AS price_per_sms                                                                                                   " +
		        			"         FROM t_asms_day_aggregation aggregation                                                                                                " +
		        			"         WHERE aggregation.msgtype = 'MT'                                                                                                       " +
		        			"                 AND zone_cd != 'HOME'                                                                                                          " +
		        			"         GROUP BY date_trunc('month', billing_date)                                                                                             " +
		        			"                 ,zone_cd                                                                                                                       " +
		        			"                 ,aggregation.msgtype                                                                                                           " +
		        			"                 ,country_and_operator                                                                                                          " +
		        			"         UNION ALL                                                                                                                              " +
		        			"         SELECT date_trunc('month', billing_date) AS billing_month                                                                              " +
		        			"                 ,aggregation.msgtype msgtype                                                                                                   " +
		        			"                 ,sum(rec_succesfull) AS count_of_SMS                                                                                           " +
		        			"                 ,zone_cd                                                                                                                       " +
		        			"                 ,country_and_operator                                                                                                          " +
		        			"                 ,NULL AS price_per_SMS                                                                                                         " +
		        			"         FROM t_asms_day_aggregation aggregation                                                                                                " +
		        			"         WHERE aggregation.msgtype = 'MO'                                                                                                       " +
		        			"                 AND zone_cd != 'HOME'                                                                                                          " +
		        			"         GROUP BY date_trunc('month', billing_date)                                                                                             " +
		        			"                 ,zone_cd                                                                                                                       " +
		        			"                 ,aggregation.msgtype                                                                                                           " +
		        			"                 ,country_and_operator                                                                                                          " +
		        			"         UNION ALL                                                                                                                              " +
		        			"         SELECT date_trunc('month', billing_date) AS billing_month                                                                              " +
		        			"                 ,aggregation.msgtype msgtype                                                                                                   " +
		        			"                 ,sum(rec_succesfull) AS count_of_SMS                                                                                           " +
		        			"                 ,zone_cd                                                                                                                       " +
		        			"                 ,country_and_operator                                                                                                          " +
		        			"                 ,NULL AS price_per_sms                                                                                                         " +
		        			"         FROM t_asms_day_aggregation aggregation                                                                                                " +
		        			"         WHERE aggregation.msgtype = 'PSMS'                                                                                                     " +
		        			"         GROUP BY date_trunc('month', billing_date)                                                                                             " +
		        			"                 ,zone_cd                                                                                                                       " +
		        			"                 ,aggregation.msgtype                                                                                                           " +
		        			"                 ,country_and_operator                                                                                                          " +
		        			"         ) AS MO_MT_FINAL                                                                                                                       " +
		        			" WHERE billing_month = date_trunc('month', NOW()) - '1 month'::interval                                                                         " ;

		
		        	conn = DriverManager.getConnection(url, user, password);
		            Statement sqlStatement2 = conn.createStatement();
					ResultSet myResultSet2 = sqlStatement2.executeQuery(readRecordSQL);
					XSSFWorkbook workbook = new XSSFWorkbook();
					XSSFSheet sheet = workbook.createSheet("ASMS_INVOICE_" + dateFormat.format(date));
				//	XSSFFont font = workbook.createFont();
					CellStyle style = workbook.createCellStyle();  
			            // Setting Background color  
			        //style.setFillBackgroundColor(IndexedColors.GREEN.getIndex());  
			        //style.setFillPattern(FillPatternType.BIG_SPOTS);  
					   // font.setBold(true);
					    XSSFRow rowhead1 = sheet.createRow((short) 0);
					    //sheet.addMergedRegion(new CellRangeAddress(0,0,0,6)); 
					    Cell cell =rowhead1.createCell((short) 0);
					    //cell.setCellStyle(style);
					    cell.setCellValue("ASMS INVOICE");
					    //cell.setCellStyle(style);
					    //rowhead1.setRowStyle(style);
					    XSSFRow rowhead = sheet.createRow((short) 1);
					    rowhead.setRowStyle(style);
					    rowhead.createCell((short) 0).setCellValue("billing_period");
					    rowhead.createCell((short) 1).setCellValue("msgtype");
					    rowhead.createCell((short) 2).setCellValue("count");
					    rowhead.createCell((short) 3).setCellValue("zone");
					    rowhead.createCell((short) 4).setCellValue("destination");
					    rowhead.createCell((short) 5).setCellValue("unit_price");
					    rowhead.createCell((short) 6).setCellValue("price");
					    //rowhead.setRowStyle(style);
					    
					    
					    XSSFRow rowtitle = sheet.createRow((short) 0);
					    rowtitle.createCell((short)0).setCellValue(" ASMS INVOICE");
					    int i = 2;
					    double total_price_fin = 0;
					    double total_price = 0;
					while (myResultSet2.next()) {
						XSSFRow row = sheet.createRow((short) i);
				        row.createCell((short) 0).setCellValue(myResultSet2.getString("billing_period"));
				        row.createCell((short) 1).setCellValue(myResultSet2.getString("msgtype"));
				        row.createCell((short) 2).setCellValue(myResultSet2.getString("count"));
				        row.createCell((short) 3).setCellValue(myResultSet2.getString("zone"));
				        row.createCell((short) 4).setCellValue(myResultSet2.getString("destination"));
				       	row.createCell((short) 5).setCellValue(Double.parseDouble(myResultSet2.getString("unit_price")));
				        
				        if (myResultSet2.getString("price") != null){
				        row.createCell((short) 6).setCellValue(Double.parseDouble(myResultSet2.getString("price")));
				        }else {
				        	row.createCell((short) 6).setCellValue(Double.parseDouble("0"));
				        }
				       
				        if(myResultSet2.getString("price") != null ){
				        	total_price = Double.parseDouble(myResultSet2.getString("price"));
				        }else{
				        	total_price = 0;
				        }
				       
				        total_price_fin = total_price_fin+ total_price;
				         
				        i++;
					}
					int num = sheet.getLastRowNum();
					XSSFRow row = sheet.createRow((short) num +2);
					row.createCell((short)5).setCellValue("total_price");
					row.createCell((short)6).setCellValue(total_price_fin);
				    System.out.println("asms total price: " + total_price_fin);
					  String yemi = "REPORTS/ASMS_INVOICE_" +dateFormat.format(date) +".xlsx";
					    FileOutputStream fileOut = new FileOutputStream(yemi);
					    workbook.write(fileOut);
					    fileOut.close();
					    myResultSet2.close();
					    
					   
					conn.close();
					
					
					sheet.autoSizeColumn(0); 
					sheet.autoSizeColumn(1);
					sheet.autoSizeColumn(2); 
					sheet.autoSizeColumn(3); 
					sheet.autoSizeColumn(4); 
					sheet.autoSizeColumn(5); 
					sheet.autoSizeColumn(6); 
					sheet.autoSizeColumn(7); 
					workbook.close();
		            
		        } catch (SQLException e1) {
		            e1.printStackTrace();
		        } catch (FileNotFoundException e1) {
		            e1.printStackTrace();
		        } catch (IOException e1) {
		            e1.printStackTrace();
		        }
		        return conn;
		    }
		    
	}
		 



