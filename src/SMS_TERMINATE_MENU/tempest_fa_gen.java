package SMS_TERMINATE_MENU;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class tempest_fa_gen {

	    public static Connection connect() {
	    	Properties result = null;
			InputStream in = null;
			in = ClassLoader.getSystemClassLoader().getResourceAsStream("db.properties");

			if (in != null) {
				result = new Properties();
				try {
					result.load(in);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} // Can throw IOException

			}

			String pguser = result.get("postgresuser").toString();
			String pwd = result.get("postgrespw").toString();
			String host = result.get("postgeshost").toString();
			
			String url = "jdbc:postgresql://"+ host +"/pgict1";
			String user = pguser;
			String password = pwd;
	        Connection conn = null;
	        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
			Date date = new Date();
	        
	        
	        try {
	        	String readRecordSQL =

" SELECT to_char(cast(MO_MT_FINAL.billing_month AS DATE),'mm.yyyy') as billing_period                                                                     " +
"         ,msgtype                                                                                                                                        " +
"         ,count_of_SMS as count                                                                                                                          " +
"         ,zone_cd as zone                                                                                                                                " +
"         ,price_per_sms as unit_price                                                                                                                    " +
"         ,fixed_fee as monthly_fee                                                                                                                       " +
"         ,round((MO_MT_FINAL.count_of_SMS * MO_MT_FINAL.price_per_sms + fixed_fee),2) AS total_price                                                     " +
" FROM (                                                                                                                                                  " +
"         SELECT date_trunc('month', billing_date) AS billing_month                                                                                       " +
"                 ,aggregation.msgtype msgtype                                                                                                            " +
"                 ,sum(rec_succesfull) AS count_of_SMS                                                                                                    " +
"                 ,zone_cd                                                                                                                                " +
"                 ,CASE                                                                                                                                   " +
"                         WHEN sum(rec_succesfull) <= 5000                                                                                                " +
"                                 THEN (                                                                                                                  " +
"                                                 SELECT poplatok_za_odoslanu_sms_v_ramci_sr                                                              " +
"                                                 FROM pgict1oper.t_temepest_pricelist pricelist                                                          " +
"                                                 WHERE variant = 0                                                                                       " +
"                                                         AND objem_sms_prevadzky = '0 - 5000'                                                            " +
"                                                 )                                                                                                       " +
"                         WHEN sum(rec_succesfull) > 5000                                                                                                 " +
"                                 AND sum(rec_succesfull) <= 50000                                                                                        " +
"                                 THEN (                                                                                                                  " +
"                                                 SELECT poplatok_za_odoslanu_sms_v_ramci_sr                                                              " +
"                                                 FROM pgict1oper.t_temepest_pricelist pricelist                                                          " +
"                                                 WHERE variant = 0                                                                                       " +
"                                                         AND objem_sms_prevadzky = '5001 - 50000'                                                        " +
"                                                 )                                                                                                       " +
"                         WHEN sum(rec_succesfull) > 10000                                                                                                " +
"                                 AND sum(rec_succesfull) <= 20000                                                                                        " +
"                                 THEN (                                                                                                                  " +
"                                                 SELECT poplatok_za_odoslanu_sms_v_ramci_sr                                                              " +
"                                                 FROM pgict1oper.t_temepest_pricelist pricelist                                                          " +
"                                                 WHERE variant = 0                                                                                       " +
"                                                         AND objem_sms_prevadzky = '50001<'                                                              " +
"                                                 )                                                                                                       " +
"                         END AS price_per_sms                                                                                                            " +
"                 ,CASE                                                                                                                                   " +
"                         WHEN sum(rec_succesfull) <= 5000                                                                                                " +
"                                 THEN (                                                                                                                  " +
"                                                 SELECT mesacny_pausal                                                                                   " +
"                                                 FROM pgict1oper.t_temepest_pricelist pricelist                                                          " +
"                                                 WHERE variant = 0                                                                                       " +
"                                                         AND objem_sms_prevadzky = '0 - 5000'                                                            " +
"                                                 )                                                                                                       " +
"                         WHEN sum(rec_succesfull) > 5000                                                                                                 " +
"                                 AND sum(rec_succesfull) <= 50000                                                                                        " +
"                                 THEN (                                                                                                                  " +
"                                                 SELECT mesacny_pausal                                                                                   " +
"                                                 FROM pgict1oper.t_temepest_pricelist pricelist                                                          " +
"                                                 WHERE variant = 0                                                                                       " +
"                                                         AND objem_sms_prevadzky = '5001 - 50000'                                                        " +
"                                                 )                                                                                                       " +
"                         WHEN sum(rec_succesfull) > 10000                                                                                                " +
"                                 AND sum(rec_succesfull) <= 20000                                                                                        " +
"                                 THEN (                                                                                                                  " +
"                                                 SELECT mesacny_pausal                                                                                   " +
"                                                 FROM pgict1oper.t_temepest_pricelist pricelist                                                          " +
"                                                 WHERE variant = 0                                                                                       " +
"                                                         AND objem_sms_prevadzky = '50001<'                                                              " +
"                                                 )                                                                                                       " +
"                         END AS fixed_fee                                                                                                                " +
"         FROM tempest_day_aggregation aggregation                                                                                                        " +
"         WHERE aggregation.msgtype = 'MT'                                                                                                                " +
"                 AND country_cd = 'SK'                                                                                                                   " +
"         GROUP BY date_trunc('month', billing_date)                                                                                                      " +
"                 ,zone_cd                                                                                                                                " +
"                 ,aggregation.msgtype                                                                                                                    " +
"         UNION ALL                                                                                                                                       " +
"         SELECT date_trunc('month', billing_date) AS billing_month                                                                                       " +
"                 ,aggregation.msgtype msgtype                                                                                                            " +
"                 ,sum(rec_succesfull) AS count_of_SMS                                                                                                    " +
"                 ,zone_cd                                                                                                                                " +
"                 ,CASE                                                                                                                                   " +
"                         WHEN sum(rec_succesfull) <= 5000                                                                                                " +
"                                 THEN (                                                                                                                  " +
"                                                 SELECT poplatok_za_prijatu_sms                                                                          " +
"                                                 FROM pgict1oper.t_temepest_pricelist pricelist                                                          " +
"                                                 WHERE variant = 0                                                                                       " +
"                                                         AND objem_sms_prevadzky = '0 - 5000'                                                            " +
"                                                 )                                                                                                       " +
"                         WHEN sum(rec_succesfull) > 5000                                                                                                 " +
"                                 AND sum(rec_succesfull) <= 50000                                                                                        " +
"                                 THEN (                                                                                                                  " +
"                                                 SELECT poplatok_za_prijatu_sms                                                                          " +
"                                                 FROM pgict1oper.t_temepest_pricelist pricelist                                                          " +
"                                                 WHERE variant = 0                                                                                       " +
"                                                         AND objem_sms_prevadzky = '5001 - 50000'                                                        " +
"                                                 )                                                                                                       " +
"                         WHEN sum(rec_succesfull) > 10000                                                                                                " +
"                                 AND sum(rec_succesfull) <= 20000                                                                                        " +
"                                 THEN (                                                                                                                  " +
"                                                 SELECT poplatok_za_prijatu_sms                                                                          " +
"                                                 FROM pgict1oper.t_temepest_pricelist pricelist                                                          " +
"                                                 WHERE variant = 0                                                                                       " +
"                                                         AND objem_sms_prevadzky = '50001<'                                                              " +
"                                                 )                                                                                                       " +
"                         END AS price_per_SMS                                                                                                            " +
"                 ,NULL AS fixed_fee                                                                                                                      " +
"         FROM tempest_day_aggregation aggregation                                                                                                        " +
"         WHERE aggregation.msgtype = 'MO'                                                                                                                " +
"         AND zone_cd = 'HOME'                                                                                                                            " +
"         GROUP BY date_trunc('month', billing_date)                                                                                                      " +
"                 ,zone_cd                                                                                                                                " +
"                 ,aggregation.msgtype                                                                                                                    " +
"         UNION ALL                                                                                                                                       " +
"         SELECT date_trunc('month', billing_date) AS billing_month                                                                                       " +
"                 ,aggregation.msgtype msgtype                                                                                                            " +
"                 ,sum(rec_succesfull) AS count_of_SMS                                                                                                    " +
"                 ,zone_cd                                                                                                                                " +
"                 ,CASE                                                                                                                                   " +
"                         WHEN zone_cd = 'ZONE1'                                                                                                          " +
"                                 THEN (                                                                                                                  " +
"                                                 SELECT t_ocs_sms_conf_rates.price / 1.2                                                                 " +
"                                                 FROM pgict1oper.t_ocs_sms_conf_rates t_ocs_sms_conf_rates                                               " +
"                                                 WHERE t_ocs_sms_conf_rates.lookup_dest = 'HOME'                                                         " +
"                                                         AND t_ocs_sms_conf_rates.network_service_desc = 'MO SMS International Zone 1'                   " +
"                                                 )                                                                                                       " +
"                         WHEN zone_cd = 'ZONE2'                                                                                                          " +
"                                 THEN (                                                                                                                  " +
"                                                 SELECT t_ocs_sms_conf_rates.price / 1.2                                                                 " +
"                                                 FROM pgict1oper.t_ocs_sms_conf_rates t_ocs_sms_conf_rates                                               " +
"                                                 WHERE t_ocs_sms_conf_rates.zone_cd = 'HOME'                                                             " +
"                                                         AND t_ocs_sms_conf_rates.network_service_desc = 'MO SMS International Zone 2'                   " +
"                                                 )                                                                                                       " +
"                         WHEN zone_cd = 'ZONE3'                                                                                                          " +
"                                 THEN (                                                                                                                  " +
"                                                 SELECT t_ocs_sms_conf_rates.price / 1.2                                                                 " +
"                                                 FROM pgict1oper.t_ocs_sms_conf_rates t_ocs_sms_conf_rates                                               " +
"                                                 WHERE t_ocs_sms_conf_rates.zone_cd = 'HOME'                                                             " +
"                                                         AND t_ocs_sms_conf_rates.network_service_desc = 'MO SMS International Zone 3'                   " +
"                                                 )                                                                                                       " +
"                         WHEN zone_cd = 'ZONE4'                                                                                                          " +
"                                 THEN (                                                                                                                  " +
"                                                 SELECT t_ocs_sms_conf_rates.price / 1.2                                                                 " +
"                                                 FROM pgict1oper.t_ocs_sms_conf_rates t_ocs_sms_conf_rates                                               " +
"                                                 WHERE t_ocs_sms_conf_rates.zone_cd = 'HOME'                                                             " +
"                                                         AND t_ocs_sms_conf_rates.network_service_desc = 'MO SMS International Zone 4'                   " +
"                                                 )                                                                                                       " +
"                         END AS price_per_sms                                                                                                            " +
"                 ,'0' AS fixed_fee                                                                                                                       " +
"         FROM tempest_day_aggregation aggregation                                                                                                        " +
"         WHERE aggregation.msgtype = 'MT'                                                                                                                " +
"                 AND zone_cd != 'HOME'                                                                                                                   " +
"         GROUP BY date_trunc('month', billing_date)                                                                                                      " +
"                 ,zone_cd                                                                                                                                " +
"                 ,aggregation.msgtype                                                                                                                    " +
"         UNION ALL                                                                                                                                       " +
"         SELECT date_trunc('month', billing_date) AS billing_month                                                                                       " +
"                 ,aggregation.msgtype msgtype                                                                                                            " +
"                 ,sum(rec_succesfull) AS count_of_SMS                                                                                                    " +
"                 ,zone_cd                                                                                                                                " +
"                 ,CASE                                                                                                                                   " +
"                         WHEN sum(rec_succesfull) <= 5000                                                                                                " +
"                                 THEN (                                                                                                                  " +
"                                                 SELECT pricelist.poplatok_za_prijatu_sms                                                                " +
"                                                 FROM pgict1oper.t_temepest_pricelist pricelist                                                          " +
"                                                 WHERE variant = 0                                                                                       " +
"                                                         AND objem_sms_prevadzky = '0 - 5000'                                                            " +
"                                                 )                                                                                                       " +
"                         WHEN sum(rec_succesfull) > 5000                                                                                                 " +
"                                 AND sum(rec_succesfull) <= 50000                                                                                        " +
"                                 THEN (                                                                                                                  " +
"                                                 SELECT pricelist.poplatok_za_prijatu_sms                                                                " +
"                                                 FROM pgict1oper.t_temepest_pricelist pricelist                                                          " +
"                                                 WHERE variant = 0                                                                                       " +
"                                                         AND objem_sms_prevadzky = '5001 - 50000'                                                        " +
"                                                 )                                                                                                       " +
"                         WHEN sum(rec_succesfull) > 10000                                                                                                " +
"                                 AND sum(rec_succesfull) <= 20000                                                                                        " +
"                                 THEN (                                                                                                                  " +
"                                                 SELECT pricelist.poplatok_za_prijatu_sms                                                                " +
"                                                 FROM pgict1oper.t_temepest_pricelist pricelist                                                          " +
"                                                 WHERE variant = 0                                                                                       " +
"                                                         AND objem_sms_prevadzky = '50001<'                                                              " +
"                                                 )                                                                                                       " +
"                         END AS price_per_SMS                                                                                                            " +
"                 ,NULL AS fixed_fee                                                                                                                      " +
"         FROM tempest_day_aggregation aggregation                                                                                                        " +
"         WHERE aggregation.msgtype = 'MO'                                                                                                                " +
"                 AND zone_cd != 'HOME'                                                                                                                   " +
"         GROUP BY date_trunc('month', billing_date)                                                                                                      " +
"                 ,zone_cd                                                                                                                                " +
"                 ,aggregation.msgtype                                                                                                                    " +
"         UNION ALL                                                                                                                                       " +
"         SELECT date_trunc('month', billing_date) AS billing_month                                                                                       " +
"                 ,aggregation.msgtype msgtype                                                                                                            " +
"                 ,sum(rec_succesfull) AS count_of_SMS                                                                                                    " +
"                 ,zone_cd                                                                                                                                " +
"                 ,NULL AS price_per_sms                                                                                                                  " +
"                 ,'0' AS fixed_fee                                                                                                                       " +
"         FROM tempest_day_aggregation aggregation                                                                                                        " +
"         WHERE aggregation.msgtype = 'PSMS'                                                                                                              " +
"         GROUP BY date_trunc('month', billing_date)                                                                                                      " +
"                 ,zone_cd                                                                                                                                " +
"                 ,aggregation.msgtype                                                                                                                    " +
"         ) AS MO_MT_FINAL                                                                                                                                " +
" WHERE billing_month = date_trunc('month', NOW()) - '1 month'::interval                                                                                  " ;

	        	conn = DriverManager.getConnection(url, user, password);
	            Statement sqlStatement2 = conn.createStatement();
				ResultSet myResultSet2 = sqlStatement2.executeQuery(readRecordSQL);
				XSSFWorkbook workbook = new XSSFWorkbook();
				XSSFSheet sheet = workbook.createSheet("TEMPEST_" + dateFormat.format(date));
				XSSFFont font = workbook.createFont();
				    font.setBold(true);
				    XSSFRow rowhead1 = sheet.createRow((short) 0);
				    rowhead1.createCell((short) 0).setCellValue("TEMPEST INVOICE");
				    XSSFRow rowhead = sheet.createRow((short) 1);
				    rowhead.createCell((short) 0).setCellValue("billing_period");
				    rowhead.createCell((short) 1).setCellValue("msgtype");
				    rowhead.createCell((short) 2).setCellValue("count");
				    rowhead.createCell((short) 3).setCellValue("zone");
				    rowhead.createCell((short) 4).setCellValue("unit_price");
				    rowhead.createCell((short) 5).setCellValue("monthly_fee");
				    rowhead.createCell((short) 6).setCellValue("total_price");
				    
				    int i = 2;
				    double total_price_fin = 0;
				    double total_price = 0;
				while (myResultSet2.next()) {
					XSSFRow row = sheet.createRow((short) i);
			        row.createCell((short) 0).setCellValue(myResultSet2.getString("billing_period"));
			        row.createCell((short) 1).setCellValue(myResultSet2.getString("msgtype"));
			        row.createCell((short) 2).setCellValue(myResultSet2.getString("count"));
			        row.createCell((short) 3).setCellValue(myResultSet2.getString("zone"));
			        row.createCell((short) 4).setCellValue(Double.parseDouble(myResultSet2.getString("unit_price")));
			        if (myResultSet2.getString("monthly_fee") != null){
			        	row.createCell((short) 5).setCellValue(Double.parseDouble(myResultSet2.getString("monthly_fee")));	
			        	
			        }else{
			        	row.createCell((short) 5).setCellValue(Double.parseDouble("0"));
			        }
			        
			        if (myResultSet2.getString("total_price") != null){
			        row.createCell((short) 6).setCellValue(Double.parseDouble(myResultSet2.getString("total_price")));
			        }else {
			        	row.createCell((short) 6).setCellValue(Double.parseDouble("0"));
			        }
			       
			        if(myResultSet2.getString("total_price") != null ){
			        	total_price = Double.parseDouble(myResultSet2.getString("total_price"));
			        }else{
			        	total_price = 0;
			        }
			       
			        total_price_fin = total_price_fin+ total_price;
			         
			        i++;
				}
				int num = sheet.getLastRowNum();
				XSSFRow row = sheet.createRow((short) num +2);
				row.createCell((short)5).setCellValue("total_price");
				row.createCell((short)6).setCellValue(total_price_fin);
			    System.out.println("tempest total price: " + total_price_fin);
				  String yemi = "REPORTS/TEMPEST_INVOICE_" +dateFormat.format(date) +".xlsx";
				    FileOutputStream fileOut = new FileOutputStream(yemi);
				    workbook.write(fileOut);
				    fileOut.close();
				    myResultSet2.close();
				    workbook.close();
				    conn.close();
	            
	        } catch (SQLException e1) {
	            e1.printStackTrace();
	        } catch (FileNotFoundException e1) {
	            e1.printStackTrace();
	        } catch (IOException e1) {
	            e1.printStackTrace();
	        }
	 
	        return conn;
	    }
	    
}
