package SMS_TERMINATE_MENU;

import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.mail.internet.MimeMultipart;

public class sendMail {
	public sendMail(){};
	

     public void sendMailReports () throws Exception {
    	String attachmentName = "REPORTS/test.file";

        Properties props = new Properties();
        props.put("mail.smtp.host", "217.75.72.70");
        props.put("mail.smtp.port", "587");
        props.put("mail.debug", "true");
        props.put("mail.smtp.starttls.enable","true");
        props.put("mail.smtp.ssl.trust","217.75.72.70");
        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress("Janko.Kubica@swanmobile.sk"));
        message.setRecipient(RecipientType.TO, new InternetAddress("Janko.Kubica@swanmobile.sk"));
        message.setSubject("Notification");
        message.setText("Successful!", "UTF-8"); // as "text/plain"
        message.setSentDate(new Date());
        
        // Set the email msg text.
        MimeBodyPart messagePart = new MimeBodyPart();
        messagePart.setText("body text");

        // Set the email attachment file
        FileDataSource fileDataSource = new FileDataSource(attachmentName);

        MimeBodyPart attachmentPart = new MimeBodyPart();
        attachmentPart.setDataHandler(new DataHandler(fileDataSource));
        attachmentPart.setFileName(fileDataSource.getName());

        // Create Multipart E-Mail.
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messagePart);
        multipart.addBodyPart(attachmentPart);  
        message.setContent(multipart);
        //Transport transport = smtpSession.getTransport("smtp");
        Transport.send(message);
    }

}
